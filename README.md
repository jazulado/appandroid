# App android

Esta es la mejor app de android y la hice sin saber java 

## Installation

para instalar haz clic [aqui](https://pip.pypa.io/en/stable/).

```bash
pip install foobar
```

## Uso

```android
import foobar

android.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Contribuciones
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update teasts as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)